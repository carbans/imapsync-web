from flask import Flask, render_template, flash, redirect
from forms import SyncForm
import subprocess

app = Flask(__name__)
app.config['SECRET_KEY'] = 'SoQu/v1gf39Orw=='


@app.route("/", methods=['GET', 'POST'])
def main():
    form = SyncForm()
    if form.validate_on_submit():
        flash('Sync requested for email {}, and password {}'.format(form.email.data, form.password.data))
        return redirect('/sync')

    try:
        
    return render_template('index.html', form=form)


@app.route("/sync")
def sync():
    return render_template('sync.html')


if __name__ == '__main__':
    app.run(debug=True)
