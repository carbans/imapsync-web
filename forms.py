from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField
from wtforms.validators import DataRequired


class SyncForm(FlaskForm):
    servidor1 = StringField('servidor1', validators=[DataRequired()])
    email1 = StringField('email1', validators=[DataRequired()])
    password1 = PasswordField('password1', validators=[DataRequired()])
    servidor2 = StringField('servidor2', validators=[DataRequired()])
    email2 = StringField('email2', validators=[DataRequired()])
    password2 = PasswordField('password2', validators=[DataRequired()])
    submit = SubmitField('Enviar')

